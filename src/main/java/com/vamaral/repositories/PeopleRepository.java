package com.vamaral.repositories;

import com.vamaral.domains.People;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PeopleRepository extends JpaRepository<People, Long> {
    List<People> findByName(String name);
}
