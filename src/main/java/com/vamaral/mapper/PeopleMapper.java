package com.vamaral.mapper;

import com.vamaral.domains.People;
import com.vamaral.requests.PeoplePostRequestBody;
import com.vamaral.requests.PeoplePutRequestBody;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public abstract class PeopleMapper {
    public static final PeopleMapper INSTANCE = Mappers.getMapper(PeopleMapper.class);
    public abstract People toPeople(PeoplePostRequestBody peoplePostRequestBody);
    public abstract People toPeople(PeoplePutRequestBody peoplePutRequestBody);
}
