package com.vamaral.config;

import com.vamaral.domains.People;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Log4j2
public class SpringClient {
    public static void main(String[] args) {
//        ResponseEntity<People> entity = new RestTemplate().getForEntity("http://localhost:8080/people/{id}", People.class, 2);
//        log.info(entity);
//
//        People object = new RestTemplate().getForObject("http://localhost:8080/people/{id}", People.class, 2);
//        log.info(object);
//
//        People[] people = new RestTemplate().getForObject("http://localhost:8080/people/all", People[].class);
//        log.info(Arrays.toString(people));
//
//
//        ResponseEntity<List<People>> exchange = new RestTemplate().exchange("http://localhost:8080/people/all",
//                HttpMethod.GET,
//                null,
//                new ParameterizedTypeReference<List<People>>() {});
//        log.info(exchange.getBody());
//
//        People kingdon = People.builder().name("kingdon").build();
//        People kingdonSaved = new RestTemplate().postForObject("http://localhost:8080/people", kingdon, People.class);
//        log.info("People Saved{}", kingdonSaved);
//
//        People profGirafales = People.builder().name("Professor Girafales").build();
//        ResponseEntity<People> profGirafalesSaved = new RestTemplate().exchange("http://localhost:8080/people",
//                HttpMethod.POST,
//                new HttpEntity<>(profGirafales),
//                People.class);
//        log.info("saved People{}", profGirafalesSaved);
//
//        People peopleUpDated =  profGirafalesSaved.getBody();
//        peopleUpDated.setName("Profe Girafalles");
//
//        ResponseEntity<Void> profGirafalesSavedUp = new RestTemplate().exchange("http://localhost:8080/people/{id}",
//                HttpMethod.PUT,
//                new HttpEntity<>(peopleUpDated),
//                Void.class);
//        log.info(profGirafalesSavedUp);
//
//        ResponseEntity<Void> profGirafalesDelet = new RestTemplate().exchange("http://localhost:8080/people/{id}",
//                HttpMethod.DELETE,
//                null,
//                Void.class,
//                peopleUpDated.getId());
//
//        log.info(profGirafalesDelet);
//
//

    }
}
