package com.vamaral.controllers;

import com.vamaral.domains.People;
import com.vamaral.requests.PeoplePostRequestBody;
import com.vamaral.requests.PeoplePutRequestBody;
import com.vamaral.services.PeopleService;
import com.vamaral.util.DateUtil;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("people")
@Log4j2
@RequiredArgsConstructor
@Builder
public class PeopleController {

    private final DateUtil dateUtil;
    private final PeopleService peopleService;


    @GetMapping
    public ResponseEntity<Page<People>> list(Pageable pageable){
        log.info(dateUtil.formaLocalDateTimeDatabaseStyle(LocalDateTime.now()));
        return ResponseEntity.ok(peopleService.listAll(pageable));
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<People>> listAll(){
        log.info(dateUtil.formaLocalDateTimeDatabaseStyle(LocalDateTime.now()));
    return ResponseEntity.ok(peopleService.listAllNomPageable());
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<People> findById(@PathVariable Long id){
    return ResponseEntity.ok(peopleService.findByIdThrowRequestExcepetion(id));
    }
    @GetMapping(path = "/find")
    public ResponseEntity<List<People>> findByName(@RequestParam String name){
        return ResponseEntity.ok(peopleService.findByName(name));
    }

    @PostMapping
    public ResponseEntity<People> save(@RequestBody @Valid PeoplePostRequestBody peoplePostRequestBody){
        return new ResponseEntity<>(peopleService.save(peoplePostRequestBody), HttpStatus.CREATED);
    }
    @DeleteMapping
    public ResponseEntity<Void> delete(@PathVariable Long id){
        peopleService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @PutMapping
    public ResponseEntity<Void> replace(@RequestBody PeoplePutRequestBody peoplePutRequestBody){
        peopleService.replace(peoplePutRequestBody);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}
