package com.vamaral.requests;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class PeoplePostRequestBody {

    @NotEmpty(message = "Please, insert name!")
    private String name;


}
