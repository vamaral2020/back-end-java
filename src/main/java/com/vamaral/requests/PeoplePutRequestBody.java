package com.vamaral.requests;

import lombok.Data;

@Data
public class PeoplePutRequestBody {

    private Long id;
    private String name;


}
