package com.vamaral.services;

import com.vamaral.domains.People;
import com.vamaral.exception.BadRequestException;
import com.vamaral.mapper.PeopleMapper;
import com.vamaral.repositories.PeopleRepository;
import com.vamaral.requests.PeoplePostRequestBody;
import com.vamaral.requests.PeoplePutRequestBody;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PeopleService {

    private final PeopleRepository peopleRepository;

    public Page<People> listAll(Pageable pageable){
        return peopleRepository.findAll(pageable);
    }
    public People findByIdThrowRequestExcepetion(Long id){
        return peopleRepository.findById(id).orElseThrow(()-> new BadRequestException("People not found!"));
    }

    public List<People> listAllNomPageable(){
       return peopleRepository.findAll();
    }
    public List<People> findByName(String name){
        return peopleRepository.findByName(name);

    }



    @Transactional
    public People save(PeoplePostRequestBody peoplePostRequestBody){
        return peopleRepository.save(PeopleMapper.INSTANCE.toPeople(peoplePostRequestBody));
        //return peopleRepository.save(new People(peoplePostRequestBody.getName()));
    }
    public void delete(long id){
        peopleRepository.delete(findByIdThrowRequestExcepetion(id));
    }
    public void replace(PeoplePutRequestBody peoplePutRequestBody){
        People savedPeople = findByIdThrowRequestExcepetion(peoplePutRequestBody.getId());
        People people = PeopleMapper.INSTANCE.toPeople(peoplePutRequestBody);
        people.setId(savedPeople.getId());
        peopleRepository.save(people);
    }


}


