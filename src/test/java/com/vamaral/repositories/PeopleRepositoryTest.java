package com.vamaral.repositories;

import com.vamaral.domains.People;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.OptionalAssert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Optional;


@DataJpaTest
@DisplayName("Test for people repository")
class PeopleRepositoryTest {
    @Autowired
    private PeopleRepository peopleRepository;

    private People createPeople() {
        return People.builder()
                .name("Wagnão")
                .build();
    }

    @Test
    @DisplayName("Save Persistence people when successful")
    void save_PersistencePeople_WhenSuccessful(){
        People peopleToBeSaved = createPeople();
        People peopleSaved = this.peopleRepository.save(peopleToBeSaved);

        Assertions.assertThat(peopleSaved).isNotNull();
        Assertions.assertThat(peopleSaved.getId()).isNotNull();
        Assertions.assertThat(peopleSaved.getName()).isEqualTo(peopleToBeSaved.getName());

    }
    @Test
    @DisplayName("Save update people when sucessesful")
    void save_UpdatePersistencePeople_WhenSucessfull(){
        People peopleToBeSaved = createPeople();
        People peopleSaved = this.peopleRepository.save(peopleToBeSaved);

        peopleSaved.setName("Wagninho");

        People peopleUpdate = this.peopleRepository.save(peopleSaved);

        Assertions.assertThat(peopleUpdate).isNotNull();
        Assertions.assertThat(peopleUpdate.getId()).isNotNull();
        Assertions.assertThat(peopleUpdate.getName()).isEqualTo(peopleSaved.getName());
    }

    @Test
    @DisplayName("Delete remove people when successfull")
    void delete_RemovePeople_WhenSuccessful(){
        People peopleToBeSaved = createPeople();
        People peopleSaved = this.peopleRepository.save(peopleToBeSaved);

        this.peopleRepository.delete(peopleSaved);

        Optional<People> peopleOptional = this.peopleRepository.findById(peopleSaved.getId());
        final OptionalAssert<People> empty;
        empty = Assertions.assertThat(peopleOptional);
        empty.isEmpty();
    }
    @Test
    @DisplayName("find by name returns list of people when successfull")
    void  findByName_ReturnsListOfPeople_WhenSuccessfull(){
        People peopleToBeSaved = createPeople();
        People peopleSaved = this.peopleRepository.save(peopleToBeSaved);

        String name = peopleSaved.getName();

        List<People> peoples = this.peopleRepository.findByName(name);

        Assertions.assertThat(peoples)
                .isNotEmpty()
                .contains(peopleSaved);
    }
    @Test
    @DisplayName("find by name returns list when no people is found")
    void findByName_ReturnsEmptyListPeople_WhenPeopleIsNotFound(){

        List<People> people = this.peopleRepository.findByName("sara");

        Assertions.assertThat(people).isEmpty();
    }

    @Test
    @DisplayName("Save throw ConstraintViolationException when successful ")
    void save_throwConstraintViolationException_WhenNameIsEmpty(){
        People people = new People();

//        Assertions.assertThatThrownBy(()-> this.peopleRepository.save(people))
//                .isInstanceOf(ConstraintViolationException.class);

        Assertions.assertThatExceptionOfType(ConstraintViolationException.class)
                .isThrownBy(()->this.peopleRepository.save(people))
                .withMessageContaining("Please, insert name!");
    }





}